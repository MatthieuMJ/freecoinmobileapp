import React from 'react';
import { View, StatusBar } from 'react-native';
import StartingComponent from './Components/StartingComponent';
import HomeComponent from './Components/HomeComponent';
import EthereumConnexion from './ExternalDataLayer/EthereumConnexion';
import FreeToken from './ExternalDataLayer/FreeToken';
import './global';

export default class App extends React.Component {
  constructor() {
    super();
    EthereumConnexion.Init();
    this.state = {
      latestBlock: {},
    }
  }

  afterConnexion() {
    this.forceUpdate();
  }

  renderTab() {
    if (FreeToken.GetInstance() !== undefined) {
      return (
        <HomeComponent/>
      );
    }
    
    return (
      <StartingComponent afterConnexion={() => this.afterConnexion()}/>
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar hidden={true}/>
        {this.renderTab()}
      </View>
    );
  }
}
