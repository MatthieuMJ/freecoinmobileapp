import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import * as Styles from '../Styles/Style';

export default class ActivityIndicatorComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.activityOn) {
            return (
                <View style={styles.activityIndicatorView}>
                    <ActivityIndicator size="large" color="#3498db" />
                </View>
            );
        }
        return (
            <View/>
        )
    }
}

const styles = Styles.style;