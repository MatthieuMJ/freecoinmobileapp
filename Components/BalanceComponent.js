import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import FreeToken from './../ExternalDataLayer/FreeToken';
import qrcode from 'yaqrcode';
import * as Styles from '../Styles/Style';

export default class BalanceComponent extends React.Component {
    static navigationOptions = {
        title: "Balance",
    };

    constructor(props) {
        super(props);
        this.state = {
            balance: 0,
            qrcodedata: String(qrcode(FreeToken.GetInstance().accountAddress,{
                size: 200
            })),
        };
    }

    componentDidMount() {
        this.getAccountBalance();
    }

    getAccountBalance() {
        FreeToken.BalanceOf(FreeToken.GetInstance().accountAddress)
        .then((balance) => {
            this.setState({balance: balance});
        })
        .catch((err) => {
            console.log(err);
        });
    }

    render() {
        this.getAccountBalance();

        return (
            <View style={styles.main}>
                <View style={styles.row}></View>
                <View style={styles.row}></View>
                <View style={styles.row}>
                    <Text style={styles.text}>{this.state.balance}</Text>
                </View>
                <View style={customStyle.interRow}></View>
                <Image style={styles.image} source={{uri: this.state.qrcodedata}}/>
            </View>
        );
    }
}

const customStyle = StyleSheet.create({
    interRow: {
        height: 100,
    }
});
const styles = Styles.style;