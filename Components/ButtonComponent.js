import React from 'react';
import { Button, View } from 'react-native';
import * as Styles from '../Styles/Style';

export default class ButtonComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title
        }
    }

    render() {
        return (
            <View style={styles.row}>
                <View style={styles.buttonView}>
                    <Button style={styles.button}
                    onPress={this.props.onPress} title={this.state.title}/>
                </View>
            </View>
        );
    }
}

const styles = Styles.style;