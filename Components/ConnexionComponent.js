import React from 'react';
import { View } from 'react-native';
import ButtonComponent from './ButtonComponent';
import ActivityIndicatorComponent from './ActivityIndicatorComponent';
import TextInputComponent from './TextInputComponent';
import UtilsServices from './../ExternalDataLayer/UtilsServices';
import FreeToken from './../ExternalDataLayer/FreeToken';
import Accounts from './../LocalDataLayer/Accounts';
import * as Styles from '../Styles/Style';

export default class ConnexionComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 'Enter your id',
            password: 'Enter your Password',
            activityOn: false
        };

        this.onIdChange = this.onIdChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    onIdChange(id) {
        this.setState({id: id});
    }

    onPasswordChange(password) {
        this.setState({password: password});
    }

    loadUser() {
        this.setState({activityOn: true});
        Accounts.Read(this.state.id)
        .then((res) => {
            FreeToken.InitInstance(UtilsServices.Decrypt(res, this.state.password).address);
            Accounts.ConnectedAccountId = this.state.id;
            this.props.afterConnexion();
        })
        .catch((err) => {
            this.setState({activityOn: false});
            console.log(err);
        });
    }

    render() {
        return (
            <View style={styles.main}>
                <View style={styles.row}></View>
                <View style={styles.row}></View>
                <TextInputComponent value={this.state.id}
                onChange={id => this.onIdChange(id)}/>
                <View style={styles.interRow}></View>
                <TextInputComponent value={this.state.password}
                secureTextEntry={true}
                onChange={password => this.onPasswordChange(password)}/>
                <View style={styles.row}></View>
                <View style={styles.row}></View>
                <ButtonComponent title="Sign In"
                onPress={this.loadUser}/>
                <ActivityIndicatorComponent activityOn={this.state.activityOn}/>
            </View>
        );
    }
}

const styles = Styles.style;

