import React from 'react';
import { View } from 'react-native';
import ButtonComponent from './ButtonComponent';
import TextInputComponent from './TextInputComponent';
import UtilsServices from './../ExternalDataLayer/UtilsServices';
import Accounts from './../LocalDataLayer/Accounts';
import './../global';
import * as Styles from '../Styles/Style';

export default class CreateAccountComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pk: 'Enter your Private Key in safe area',
            id: 'Enter your id',
            password: 'Enter your Password',
        };

        this.onPkChange = this.onPkChange.bind(this);
        this.onIdChange = this.onIdChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.createUser = this.createUser.bind(this);
    }

    onPkChange(pk) {
        this.setState({pk: pk});
    }

    onIdChange(id) {
        this.setState({id: id});
    }

    onPasswordChange(password) {
        this.setState({password: password});
    }

    createUser() {        
        Accounts.Create(this.state.id,
            UtilsServices.EncryptAsString(this.state.pk, this.state.password));
    }

    render() {
        return (
            <View style={styles.main}>
                <View style={styles.row}></View>
                <View style={styles.row}></View>
                <TextInputComponent value={this.state.pk}
                onChange={pk => this.onPkChange(pk)}/>
                <View style={styles.interRow}></View>
                <TextInputComponent value={this.state.id}
                onChange={id => this.onIdChange(id)}/>
                <View style={styles.interRow}></View>
                <TextInputComponent value={this.state.password}
                secureTextEntry={true}
                onChange={password => this.onPasswordChange(password)}/>
                <View style={styles.row}></View>
                <View style={styles.row}></View>
                <ButtonComponent title="Sign Up"
                onPress={this.createUser}/>
            </View>
        );
    }
}

const styles = Styles.style;

