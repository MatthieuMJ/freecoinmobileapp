import React from 'react';
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';
import BalanceComponent from './BalanceComponent';
import TransferComponent from './TransferComponent';

const HomeComponentNavigator = createBottomTabNavigator({
    Balance: {screen : BalanceComponent},
    Transfer: {screen : TransferComponent},
},{
    tabBarOptions:{
      activeTintColor: "#ecf0f1",
      activeBackgroundColor: "#3498db",
      inactiveBackgroundColor: "#2980b9",
      labelStyle:{
        fontSize: 24,
        fontWeight: "bold",
        paddingVertical:7,
      }
    },
});

const HomeContainer = createAppContainer(HomeComponentNavigator);

export default class HomeComponent extends React.Component {

    render() {
        return (
            <HomeContainer/>
        );
    }
}