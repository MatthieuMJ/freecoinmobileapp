import React from 'react';
import ConnexionComponent from './ConnexionComponent';
import CreateAccountComponent from './CreateAccountComponent';
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';

export default class StartingComponent extends React.Component {
    
    render() {
        const StartingComponentNavigator = createBottomTabNavigator({
            Connexion: {
                screen : () => <ConnexionComponent afterConnexion={() => this.props.afterConnexion()}/>,
                navigationOptions: () => ({
                    title: "Sign In",
                })
            },
            AccountCreation: {
                screen : CreateAccountComponent,
                navigationOptions: () => ({
                    title: "Sign Up",
                })
            },
        },{
            tabBarOptions:{
              activeTintColor: "#ecf0f1",
              activeBackgroundColor: "#3498db",
              inactiveBackgroundColor: "#2980b9",
              labelStyle:{
                fontSize: 24,
                fontWeight: "bold",
                paddingVertical:7,
              }
            },
        });

        const StartingContainer = createAppContainer(StartingComponentNavigator);

        return (
            <StartingContainer/>
        );
    }
}