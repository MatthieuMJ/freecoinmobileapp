import React from 'react';
import { TextInput, View } from 'react-native';
import * as Styles from '../Styles/Style';

export default class TextInputComponent extends React.Component {
    constructor(props) {
        super(props);
        var secureTextEntry;
        var keyboardType;

        if (props.keyboardType != undefined) {
            keyboardType = props.keyboardType;
        }
        else {
            keyboardType = "default";
        }
        if (props.secureTextEntry != undefined) {
            secureTextEntry = props.secureTextEntry;
        }
        else {
            secureTextEntry = false;
        }

        this.state = {
            secureTextEntry: secureTextEntry,
            keyboardType: keyboardType,
        }
    }

    render() {
        return (
            <View style={styles.row}>
                <TextInput style={styles.textInput}
                    value={this.props.value}
                    secureTextEntry={this.state.secureTextEntry}
                    keyboardType={this.state.keyboardType}
                    underlineColorAndroid='transparent'
                    onChangeText={value => this.props.onChange(value)}
                    selectTextOnFocus={true}/>
            </View>
        );
    }
}

const styles = Styles.style;