import React from 'react';
import { View, StyleSheet, Text, Button, TextInput } from 'react-native';
import ButtonComponent from './ButtonComponent';
import ActivityIndicatorComponent from './ActivityIndicatorComponent';
import TextInputComponent from './TextInputComponent';
import { BarCodeScanner, Permissions } from 'expo';
import UtilsServices from './../ExternalDataLayer/UtilsServices';
import FreeToken from './../ExternalDataLayer/FreeToken';
import Accounts from './../LocalDataLayer/Accounts';
import * as Styles from '../Styles/Style';

export default class TransferComponent extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            hiddenBarCode: true,
            to: '',
            amount: '',
            password: '',
            activityOn: false,
        };
        this.activateQrCode = this.activateQrCode.bind(this);
        this.handleBarCodeScanned = this.handleBarCodeScanned.bind(this);
        this.onAddressChange = this.onAddressChange.bind(this);
        this.onAmountChange = this.onAmountChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.transfer = this.transfer.bind(this);
    }

    renderBarCodeScanner() {

        if (this.state.hiddenBarCode) {
            return (
                <View>
                    <View style={customstyles.interRow}></View>
                    <View style={styles.buttonView}>
                    <ButtonComponent title="Get Receiver Address"
                    onPress={this.activateQrCode}/>
                    </View>
                </View>
            )
        }

        return (
            <View>
                <View style={customstyles.interRow}></View>
                <View style={styles.barCodeScanner}>
                    <BarCodeScanner
                        onBarCodeScanned={this.handleBarCodeScanned}
                        style={StyleSheet.absoluteFillObject}
                    />
                </View>
            </View>
        )
    }

    renderTransfer() {
        if (this.state.hiddenBarCode && UtilsServices.CheckAddress(String(this.state.to))) {
            return (
                <View>
                    <TextInputComponent value={this.state.amount}
                    keyboardType={"numeric"}
                    onChange={amount => this.onAmountChange(amount)}/>
                    <View style={customstyles.interTextInput}></View>
                    <TextInputComponent value={this.state.password}
                    secureTextEntry={true}
                    onChange={password => this.onPasswordChange(password)}/>
                    <View style={customstyles.interRow}></View>
                    <ButtonComponent title="Transfer Free Coin"
                    onPress={this.transfer}/>
                    <ActivityIndicatorComponent activityOn={this.state.activityOn}/>
                </View>
            );
        }
        return (
            <View/>
        );
    }

    onPasswordChange(password) {
        this.setState({password: password});
    }

    onAmountChange(amount) {
        this.setState({amount: amount});
    }

    onAddressChange(address) {
        this.setState({to: address});
    }

    handleBarCodeScanned = ({ type, data }) => {
        if(UtilsServices.CheckAddress(String(data))) {
            this.setState({to: data})
            this.setState({hiddenBarCode: true});
        }
        else {
            console.log(data);
        }
    }

    transfer() {
        this.setState({activityOn: true});
        Accounts.Read(Accounts.ConnectedAccountId)
        .then((res) => {
            FreeToken
            .Transfer(FreeToken.GetInstance().accountAddress,
             this.state.to, this.state.amount, UtilsServices.Decrypt(res, this.state.password).signTransaction);
             this.setState({activityOn: false});
        })
        .catch((err) => {
            console.log(err);
            this.setState({activityOn: false});
        });
    }

    activateQrCode() {
        this.setState({hiddenBarCode: false});
    }
    
    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }
    
    render() {
        const { hasCameraPermission } = this.state;
    
        if (hasCameraPermission === null) {
          return <Text>Requesting for camera permission</Text>;
        }
        if (hasCameraPermission === false) {
          return <Text>No access to camera</Text>;
        }
        return (
            <View style={styles.main}>
                <TextInputComponent value={this.state.to}
                onChange={to => this.onAddressChange(to)}/>
                {this.renderBarCodeScanner()}
                {this.renderTransfer()}
            </View>
        );
    }
}

const styles = Styles.style;

const customstyles = StyleSheet.create({
    interTextInput: {
        height: 15,
    },
    interRow: {
        height: 60,
    },
});