import './../global';
const Web3 = require('web3');

const infuraLink = 'https://ropsten.infura.io/v3/41be9e0c526142a890b75f8aeb7692bf'

export default class EthereumConnexion {
      web3;
      static Instance;

      constructor(provider) {
          this.web3 = new Web3(
            new Web3.providers.HttpProvider(provider)
          );
      }

      static Init() {
        EthereumConnexion.Instance = new EthereumConnexion(infuraLink);
      }

      static GetInstance() {
          if (EthereumConnexion.Instance == null) {
              EthereumConnexion.Instance =
              new EthereumConnexion(infuraLink);
          }
          return EthereumConnexion.Instance;
      }
  }