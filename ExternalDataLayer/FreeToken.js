import EthereumConnexion from './EthereumConnexion';
import UtilsServices from './UtilsServices';

const freeTokenJson = require('./../contracts/Free.json');
const contractAddress = '0x76c77e16268C9B4B0E7B202809fE223C1B014791';

export default class FreeToken {
    contract;
    accountAddress;
    static FreeInstance;

    constructor(accountAddress) {
        this.accountAddress =  accountAddress;
        
        this.GetContract();
    }

    static GetInstance() {
        if (FreeToken.FreeInstance == undefined) {
            console.log('Need to connect with valid user');
            return;
        }
        return FreeToken.FreeInstance;
    }

    static InitInstance(accountAddress) {
        FreeToken.FreeInstance = new FreeToken(accountAddress);
    }

    GetContract() {
        if(!FreeToken.CheckAddress(this.accountAddress, 'Account address not valid.')
        || !FreeToken.CheckAddress(contractAddress, 'Contract address not valid.')) {
            return;
        }

        this.contract = new (EthereumConnexion.GetInstance().web3.eth)
        .Contract(freeTokenJson.abi, contractAddress, { from : this.accountAddress});
    }

    static CheckAddress(address, errorMessage) {
        var isValid = UtilsServices.CheckAddress(String(address));

        if (!isValid) {
            console.log(errorMessage);
        }

        return isValid;
    }

    static CheckEthereumConnexion() {
        if (FreeToken.GetInstance() === undefined) {
            alert('Create or Find Free Tokens.');
            return false;
        }
        return true;
    }

    static BalanceOf(tokensOwner) {
        if (!FreeToken.CheckEthereumConnexion() || !FreeToken.CheckAddress(tokensOwner, 'Account address not valid.')) {
            return;
        }

        return FreeToken.GetInstance().contract.methods.balanceOf(tokensOwner).call();
    }

    static Transfer(tokensOwner, to, tokens, signTransaction) {
        console.log(signTransaction);
        console.log(tokensOwner);
        if (!FreeToken.CheckEthereumConnexion()
         || !FreeToken.CheckAddress(tokensOwner, 'Account address not valid.')
         || !FreeToken.CheckAddress(to, 'Receiver address not valid.')) {
            return;
        }

        var optionsGasEstimate = {
            from: tokensOwner,
        };

        return FreeToken.GetInstance().contract.methods.transfer(to, tokens)
        .estimateGas(optionsGasEstimate)
        .then((gasToPay) => {
            EthereumConnexion.GetInstance().web3.eth.getTransactionCount(tokensOwner)
            .then((nonce) => {
                var txParams = {
                    nonce: nonce,
                    from: tokensOwner,
                    to: contractAddress,
                    data: FreeToken.GetInstance().contract.methods.transfer(to, tokens).encodeABI(),
                    gas: gasToPay,
                };

                signTransaction(txParams).then((signedTx) => {
                    EthereumConnexion.GetInstance().web3.eth.sendSignedTransaction(signedTx.rawTransaction).
                    on('receipt', console.log);
                })
                .catch((err) => {
                    console.log(err);
                })
            })
            .catch((err) => {
                console.log(err);
            });
        })
        .catch((err) => {
            console.log(err);
        });
    }
}