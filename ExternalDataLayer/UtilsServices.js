import EthereumConnexion from './EthereumConnexion';
import './../global';

export default class UtilsServices {
    static CheckAddress(address) {
        return EthereumConnexion.GetInstance().web3.utils.isAddress(address);
    }

    static Encrypt(privateKey, password) {
        return EthereumConnexion.GetInstance().web3.eth.accounts.encrypt(privateKey, password);
    }

    static EncryptAsString(privateKey, password) {
        return JSON.stringify(UtilsServices.Encrypt(privateKey, password));
    }

    static Decrypt(keystoreJsonV3AsString, password) {
        return EthereumConnexion.GetInstance().web3.eth.accounts.decrypt(
            JSON.parse(keystoreJsonV3AsString), password);
    }
}