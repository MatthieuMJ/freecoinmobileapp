import { AsyncStorage } from 'react-native';

export default class Accounts { // CRUD OPERATION

    static async Create(id, encryptedPk) {
        try {
            await AsyncStorage.setItem(id, encryptedPk);
        } catch (error) {
            console.log(error);
        }
    }

    static async Read(id) {
        try {
            var encryptedPk = await AsyncStorage.getItem(id);
            if (encryptedPk === null) {
                console.log('id doesn\'t exist');
                return;
            }
            return encryptedPk;
        } catch (error) {
            console.log(error);
        }
    }
}