# Free Coin Mobile App using React-Native, Web3 and Infura

This mobile application can communicate with Free Coin smart contract. It's split in two part :

1. Sign In/Sign Up: Create/Open with your ethereum account (private key, chosen id and password, private key will be encrypted).
     
2. Balance/Transfer of Free Coin: Using Qr Code Api [yaqrcode](https://github.com/zenozeng/node-yaqrcode) for displaying and reading address.

## A Video is better than an explanation

[![](http://img.youtube.com/vi/uAc4MousaO4/0.jpg)](http://www.youtube.com/watch?v=uAc4MousaO4 "Free Coin Mobile App")

## How to run Free Coin Mobile App

Clone the repository in local :
    
`git clone git@bitbucket.org:MatthieuMJ/freecoinmobileapp.git`

Install node module :
    
`npm i`

Start the application :
    
`expo start`

