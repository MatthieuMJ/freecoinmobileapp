import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
    main: {
        flex: 1,
        flexDirection:'column',
    },
    row: {
        height: 50,
    },
    interRow: {
        height: 20,
    },
    text: {
        color: "#2980b9",
        fontSize: 100,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    image: {
        alignContent: 'center',
        alignSelf: 'center',
        minWidth: 200,
        minHeight: 200,
    },
    textInput: {
        borderWidth: 1,
        borderColor: "#3498db",
        height: 50,
        padding:10,
        margin:50,
    },
    buttonView: {
        marginLeft: 50,
        marginRight: 50,
    },
    activityIndicatorView: {
        marginLeft: 50,
        marginRight: 50,
    },
    button: {
        backgroundColor: "#2980b9",
    },
    barCodeScanner: {
        alignContent: 'center',
        alignSelf: 'center',
        minWidth: 300,
        minHeight: 300,
    }
});