// Inject node globals into React Native global scope.
global.Buffer = require('buffer').Buffer;
global.process = require('process');

if (typeof btoa === 'undefined') {
  global.btoa = function(str) {
    return new Buffer(str, 'binary').toString('base64');
  };
}

if (typeof atob === 'undefined') {
  global.atob = function(b64Encoded) {
    return new Buffer(b64Encoded, 'base64').toString('binary');
  };
}

let crypto
  if (typeof global === 'object') {
    if (!global.crypto) global.crypto = {}
    crypto = global.crypto
  } else {
    crypto = require('crypto')
  }

  if (!crypto.getRandomValues) {
    crypto.getRandomValues = getRandomValues
  }

  let randomBytes = Object

  function getRandomValues (arr) {
    if (typeof(randomBytes) === Object){
       randomBytes = require('react-native-randombytes').randomBytes
    }

    const bytes = randomBytes(arr.length)
    for (var i = 0; i < bytes.length; i++) {
      arr[i] = bytes[i]
    }
  }